package com.epam.terraBattle;


import com.epam.terraBattle.exceptions.BadNumberException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class BattleField {

  private static BattleField instance = null;
  private int[] ix;
  private int[] iy;

  private BattleField() {
    this.ix = new int[50];
    this.iy = new int[50];
    for (int i = 0; i < ix.length; i++) {
      ix[i] = i;
    }
    iy = ix;
  }

  public static BattleField getInstance() {
    if (instance == null) {
      instance = new BattleField();
    }
    return instance;
  }


  private int uniqueXRed = 0;
  private int uniqueXBlue = 0;

  public int getUnicXRed() throws BadNumberException {
    if (uniqueXRed >= ix.length) {
//      throw !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
    return uniqueXRed += 1;
  }

  public int getUnicXBlue() throws BadNumberException {
    if (uniqueXBlue >= ix.length) {
//      throw !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
    return uniqueXBlue += 1;
  }


  public int[] getIx() {
    return ix;
  }

  public int[] getIy() {
    return iy;
  }


  private Map<List<Integer>, Integer> coordinates = new HashMap<List<Integer>, Integer>();


  public Map<List<Integer>, Integer> getCoordinates() {
    return coordinates;
  }


  @Override
  public String toString() {
    return "BattleField{" +
        "ix=" + Arrays.toString(ix) +
        ", iy=" + Arrays.toString(iy) +
        ", uniqueXRed=" + uniqueXRed +
        ", uniqueXBlue=" + uniqueXBlue +
        ", coordinates=" + coordinates +
        '}';
  }
}