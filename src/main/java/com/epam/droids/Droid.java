package com.epam.droids;

import com.epam.arsenal.Weapon;

import com.epam.terraBattle.BattleField;
import com.epam.terraBattle.exceptions.BadNumberException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Droid {

  private String name;
  private int hp;
  private ColorType colorType;
  private int x;
  private int y;
  private Weapon weapon;  // empty wrapper to create normal constructor            !!!!!!!!!!!!!!!!


  public Droid(String name, ColorType colorType, Weapon weapon)
      throws BadNumberException {
    this.name = name;
    this.hp = 100;
    this.colorType = colorType;
    if (colorType == ColorType.RED) {
      this.x = BattleField.getInstance().getIx()[BattleField.getInstance().getUnicXRed()];
    } else {
      this.x = BattleField.getInstance().getIx()[BattleField.getInstance().getUnicXBlue()];
    }
    this.y = BattleField.getInstance().getIy()[colorType.getInitY()];
    this.weapon = weapon;


    List<Integer> specKey = new ArrayList<Integer>();
    specKey.add(y);
    specKey.add(x);
    BattleField.getInstance().getCoordinates().put(specKey, x);
  }




  public void letsgo() {
    List<Integer> keyYXPrex = new LinkedList();
    keyYXPrex.add(y);
    keyYXPrex.add(x);

    Scanner sc = new Scanner(System.in);

    int stepY = sc.nextInt();
    int stepX = sc.nextInt();

    int upMega = y + 5;
    int downMega = y - 5;
    int leftMega = x - 5;
    int rightMega = x + 5;

    List<Integer> keyYX = new LinkedList();
    keyYX.add(stepY);
    keyYX.add(stepX);

    Integer xVal = BattleField.getInstance().getCoordinates().get(keyYX);
    if ((xVal == null) && (stepY < upMega) && (stepY > downMega) && (stepY >= 0) && (stepY
        < BattleField.getInstance().getIy().length) &&
        (stepX < rightMega) && (stepX > leftMega) && (stepX >= 0) && (stepX < BattleField
        .getInstance().getIx().length)) {

      BattleField.getInstance().getCoordinates().remove(keyYXPrex);
      y = stepY;
      x = stepX;
      BattleField.getInstance().getCoordinates().put(keyYX, stepX);

    } else {
      System.out.println(
          "Area is not free or you've tried to leave the battleField or decided to stay at the same point !");
    }

  }


  @Override
  public String toString() {
    return "Droid{" +
        "name='" + name + '\'' +
        ", hp=" + hp +
        ", colorType=" + colorType +
        ", x=" + x +
        ", y=" + y +
        ", weapon=" + weapon +
        '}';
  }
}
