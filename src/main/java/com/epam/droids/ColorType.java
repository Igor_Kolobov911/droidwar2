package com.epam.droids;

public enum  ColorType {
  RED(49),
  BLUE(0);

  private int initY;

  ColorType(int initY) {
    this.initY = initY;
  }

  public int getInitY() {
    return initY;
  }
}
